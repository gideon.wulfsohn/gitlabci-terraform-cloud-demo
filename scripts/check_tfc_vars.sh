#!/bin/bash
# This script checks whether required TFC variables are set
# If all variables are successful, it writes a workspace_id file
rm -f workspace_id

# Check if curl and jq are installed
if ! [ -x "$(command -v jq)" ]; then
  echo "Error: jq is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi
if ! [ -x "$(command -v curl)" ]; then
  echo "Error: curl is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi

if [ ! -z "$TFC_TOKEN" ]; then
  echo "TFC_TOKEN environment variable was found."
else
  echo "ERR: TFC_TOKEN environment variable was not set."
  echo "You must export/set the TFC_TOKEN environment variable."
  echo "It should be a user or team token that has write or admin"
  echo "permission on the workspace."
  echo "Exiting."
  exit 1
fi

# Evaluate $TFC_ORG environment variable
# If not set, give error and exit
if [ ! -z "$TFC_ORG" ]; then
  echo "Using TFC Organization: ${TFC_ORG}."
else
  echo "ERR: You must export/set the TFC_ORG environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate $TFC_WORKSPACE environment variable
# If not set, give error and exit
if [ ! -z "$TFC_WORKSPACE" ]; then
  echo "Using TFC Workspace: ${TFC_WORKSPACE}."
else
  echo "ERR: You must export/set the TFC_WORKSPACE environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate that the $TFC_WORKSPACE name exist in the given organization
workspace_id=$(curl -s --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id)
if [ -z "$workspace_id" ] || [ "$workspace_id" = null ]; then
  echo "ERR: Could not find workspace named: ${TFC_WORKSPACE}."
  echo "Please ensure TFC_WORKSPACE, TFC_ORG and TFC_TOKEN is set correctly."
  exit 1
fi

echo "Workspace ID is ${workspace_id}"
echo ${workspace_id} > workspace_id